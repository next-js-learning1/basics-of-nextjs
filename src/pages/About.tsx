import Header from "@/components/Header";
import Head from "next/head";

export default function About() {
  return (
    <>
      <Head>
        <title>About</title>
      </Head>
      <Header></Header>
      <h1>About</h1>
    </>
  );
}
