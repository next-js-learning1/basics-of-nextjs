import Header from "@/components/Header";
import styles from "../styles/Home.module.css";
import { useState } from "react";

export default function Home() {

  const [firstName, setFirstName] = useState("Yassine");
  console.log(firstName);

  const addLastName = () => {
    setFirstName(prevFirstName => {
      return prevFirstName + "Yadine";
    })
  }


  return (
    <>
      <Header></Header>

      <h1>
        <span className={`${styles.icon} material-symbols-outlined`}>home</span>
        Accueil @ {firstName}
      </h1>

      <input placeholder="first name"/>
      <button onClick={() => addLastName} >Change the first name</button>
    </>
  );
}
