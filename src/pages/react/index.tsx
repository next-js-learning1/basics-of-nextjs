import Header from "@/components/Header";
import Level from "@/components/Level";
import { useState } from "react";

export default function React() {
    const [counter, setCounter] = useState(0);

   function increment() {
    setCounter(counter + 1);
   }

   function decrement() {
    setCounter(counter - 1);
   }

  return (
    <>
      <Header></Header>
      <h1>React learning</h1>
      <Level title={"Premier shot"}></Level>
      <Level title={'counter -> ' + counter.toString()}/>

      <div>
        <button onClick={increment}>Increment</button>
        <button onClick={decrement}>Decrement</button>
      </div>
    </>
  );
}
