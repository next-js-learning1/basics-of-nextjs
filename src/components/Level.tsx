interface LevelProps {
  title: string;
}

export default function Level({ title }: LevelProps) {

  return (
    <>
      <h2>{title ? title : "Un titre par defaut"}</h2>
    </>
  );
}
