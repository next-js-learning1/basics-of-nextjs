import Link from "next/link";
import React from "react";
import styles from "../styles/Header.module.css";

const Header = () => {
  return (
    <ul className={styles.menu}>
      <li><Link href="/">Accueil</Link></li>
      <li><Link href="/About">About</Link></li>
      <li><Link href="/blog">Blog</Link></li>
      <li><Link href="/react">React learning</Link></li>
    </ul>
  );
};

export default Header;
